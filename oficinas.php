<!DOCTYPE HTML>
<!--[if IE 7]><html lang="es" class="ie7 ie"><![endif]-->  
<!--[if IE 8]><html lang="es" class="ie8 ie"><![endif]-->  
<!--[if IE 9]><html lang="es" class="ie9 ie"><![endif]-->  
<!--[if !IE]><!--><html lang="es"><!--<![endif]-->  
<head>
<title>Centro Andino - Oficinas</title>
<?php include('templates/head.php'); ?>
<script type="text/javascript">
$(window).load(function(){
	
	$('#masterslider').masterslider({
		width: $(window).width(),
		height: ($(window).height()-77),
		space: 0,
		speed: 20,
		instantStartLayers: true,
		layout: 'fullwidth',
		loop: true,
		preload: 0,
		autoplay: true,
		view: 'basic',
		controls : {
			arrows : { autohide: false }
		}
	});
	
	$('div.slick-slider').slick({
		cssEase: 'ease',
		fade: false,
		arrows: true,
		infinite: true,
		dots: false,
		autoplay: true,
		autoplaySpeed: 4000,
		pauseOnHover: false,
		slidesToShow: 1,
		slidesToScroll: 1
	});
	
});
</script>
</head>
<body>

    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69585118-1', 'auto');
  ga('send', 'pageview');

</script>

	<div id="loader">
        <div class="cssload-loader"></div>
    </div>
    <!-- start: #wrapper -->
    <div id="wrapper">
        
    	<!-- start: .content -->
		<main id="proyecto-int" role="main">
        
        	<!-- start: #top -->
			<section id="top">
                <a href="tel:+56226118400" class="fono">+562 2951 9269 <span>CONVERSEMOS</span></a>
                <!-- start: #masterslider -->
                <div id="masterslider" class="master-slider ms-skin-default">
                    <!-- start: .ms-slide -->
                    <div class="ms-slide">
                        <img src="images/blank.gif" data-src="images/oficinas-slider/1.jpg">
                        <div class="ms-layer ms-caption" style="left:45px; bottom:60px;" data-type="text" data-effect="left(500)"  data-duration="1500" data-delay="900" data-ease="easeOutExpo">
                            OFICINAS EN EL MEJOR SECTOR<br>
                            <strong>DE LOS TRAPENSES</strong>
                        </div>
                    </div>
                    <!-- end: .ms-slide -->
                    <!-- start: .ms-slide -->
                    <div class="ms-slide">
                        <img src="images/blank.gif" data-src="images/oficinas-slider/2.jpg">
                        <div class="ms-layer ms-caption" style="left:45px; bottom:60px;" data-type="text" data-effect="left(500)"  data-duration="1500" data-delay="900" data-ease="easeOutExpo">
                           OFICINAS EN EL MEJOR SECTOR<br>
                            <strong>DE LOS TRAPENSES</strong>
                        </div>
                    </div>
                    <!-- end: .ms-slide -->
                     <!-- start: .ms-slide -->
                    <div class="ms-slide">
                        <img src="images/blank.gif" data-src="images/oficinas-slider/3.jpg">
                        <div class="ms-layer ms-caption" style="left:45px; bottom:60px;" data-type="text" data-effect="left(500)"  data-duration="1500" data-delay="900" data-ease="easeOutExpo">
                           <strong> OFICINAS 100% HABILITADAS</strong><br>
                            DESDE 22 m<sup>2</sup>
                        </div>
                    </div>
                    <!-- end: .ms-slide -->
                    <!-- start: .ms-slide -->
                    <div class="ms-slide">
                        <img src="images/blank.gif" data-src="images/oficinas-slider/4.jpg">
                        <div class="ms-layer ms-caption" style="left:45px; bottom:60px;" data-type="text" data-effect="left(500)"  data-duration="1500" data-delay="900" data-ease="easeOutExpo">
                            <strong> OFICINAS 100% HABILITADAS</strong><br>
                            DESDE 22 m<sup>2</sup>
                        </div>
                    </div>
                    <!-- end: .ms-slide -->
                </div>
                <!-- end: #masterslider -->
            </section>
            <!-- end: #top -->
            
            <!-- start: #header -->
            <header id="header" role="banner">
            	<div class="grid-container">
                    <h1 class="logo"><a href="index.php"><img src="images/logo.png"></a></h1>
                    <nav>
                        <ul>
                            <li><a href="index.php#proyecto">CENTRO ANDINO</a></li>
                            <li><a href="index.php#emplazamiento">EMPLAZAMIENTO</a></li>
                            <li><a href="index.php#entorno">ENTORNO</a></li>
                            <li><a href="index.php#tour">TOUR 360</a></li>
                            <li><a href="index.php#ubicacion">UBICACIÓN</a></li>
                            <li><a href="oficinas.php" class="active-oficinas">OFICINAS</a></li>
                            <li><a href="stripcenter.php">STRIP CENTER</a></li>
                        </ul>
                    </nav>
                    <div class="menu-mobile"></div>
                </div>
                <div class="clear"></div>
                <div class="submenu hide-on-tablet hide-on-mobile">
                    <div class="grid-container">
                        <ul>
                            <li><a href="#descripcion">DESCRIPCIÓN</a></li>
                            <li><a href="#galeria">GALERÍA</a></li>
                            <li><a href="#plantas">MODELOS</a></li>
                            <li><a href="#contacto">CONTACTO</a></li>
                        </ul>
                    </div>
                </div>
            </header>
            <!-- end: #header -->
            
            <!-- start: #descripcion -->
            <section id="descripcion" class="section">
            	<div class="grid-container">
                	<h2 class="titulo wow fadeInDown">DETALLES QUE MARCAN LA DIFERENCIA</h2>
               		<div class="grid-60 tablet-grid-100 wow fadeInLeft" data-wow-delay="500ms">
                    	<img src="images/oficinas-descripcion.jpg" class="fullwidth">
                    </div>
                    <div class="grid-30 tablet-grid-100  alignright wow fadeInRight" data-wow-delay="500ms">
                    	<div class="texto">
                        	<p class="light"><strong>Edificio</strong></p>
                            <ul>
                                <li>Oficinas habilitadas desde 22 m<sup>2</sup></li>
                                <li>Moderno edificio de 6 pisos.</li>
                                <li>3 pisos subterráneos.</li>
                                <li>Parking de estacionamiento.</li>
                                <li>Servicio de mini bodega.</li>
                                <li>Acceso controlado, CCTV 24 horas.</li>
                                <li>Ascensores de última generación.</li>
                                <li>Estacionamientos para bicicletas.</li>
                                <li>Amplio hall de acceso.</li>
                                <li>Placa comercial.</li>
                            </ul>
                            <p class="light"><strong>Terminaciones oficinas</strong></p>
                            <ul>
                                <li>Tabiquería incorporada.</li>
                                <li>Piso fotolaminado.</li>
                                <li>Baños terminados.</li>
                                <li>Cielo americano.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </section>
            <!-- end: #descripcion -->
            
            <!-- start: #galeria -->
            <section id="galeria" class="galeria section">
            	<div class="grid-container">
                    <h2 class="titulo wow fadeInDown">GALERÍA</h2>
                    <div class="wow fadeIn" data-wow-delay="500ms">
                        <ul class="tabs">
                            <li><a href="#">IMÁGENES</a></li>
                            <!--<li><a href="#">VIDEO</a></li> -->
                        </ul>
                        <div class="panes">
                            <!-- start: .tab -->
                            <div class="tab">
                                <div class="slick-slider">
                                    <div class="item"><a href="images/oficinas-galeria/1-hd.jpg" class="lightbox" rel="galeria"><img src="images/oficinas-galeria/1.jpg"></a></div>
                                    <div class="item"><a href="images/oficinas-galeria/2-hd.jpg" class="lightbox" rel="galeria"><img src="images/oficinas-galeria/2.jpg"></a></div>
                                    <div class="item"><a href="images/oficinas-galeria/3-hd.jpg" class="lightbox" rel="galeria"><img src="images/oficinas-galeria/3.jpg"></a></div>
                                    <div class="item"><a href="images/oficinas-galeria/4-hd.jpg" class="lightbox" rel="galeria"><img src="images/oficinas-galeria/4.jpg"></a></div>
                                    <div class="item"><a href="images/oficinas-galeria/5-hd.jpg" class="lightbox" rel="galeria"><img src="images/oficinas-galeria/5.jpg"></a></div>
                                </div>
                            </div>
                            <!-- end: .tab -->
                            <!-- start: .tab -->
                            <div class="tab">
                                <iframe width="100%" height="477" src="https://www.youtube.com/embed/HTpjzPqASfA?rel=0&showinfo=0&vq=large" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <!-- end: .tab -->
                        </div>
                    </div>
                </div>
            </section>
            <!-- end: #galeria -->
            
            <!-- start: #plantas -->
            <section id="plantas" class="section">
            	<div class="grid-container">
                    <h2 class="titulo wow fadeInDown">OFICINAS HABILITADAS DESDE 22 M<sup>2</sup></h2>
                    <div class="wow fadeIn" data-wow-delay="500ms">
                        <!-- start: .item -->
                        <div class="item grid-33 tablet-grid-50 grid-parent">
                            <a href="oficinas/planta-22.php" class="fancybox" rel="oficinas">
                                <img src="images/oficinas-plantas/22.jpg">
                                <div class="bottom">
                                    <span class="mt"><strong>22</strong> m<sup>2</sup></span>
                                    <span class="cotizar">COTIZAR</span>
                                    <div class="clear"></div>
                                </div>
                            </a>
                        </div>
                        <!-- end: .item -->
                        <!-- start: .item -->
                        <div class="item grid-33 tablet-grid-50 grid-parent">
                            <a href="oficinas/planta-27.php" class="fancybox" rel="oficinas">
                                <img src="images/oficinas-plantas/27.jpg">
                                <div class="bottom">
                                    <span class="mt"><strong>27</strong> m<sup>2</sup></span>
                                    <span class="cotizar">COTIZAR</span>
                                    <div class="clear"></div>
                                </div>
                            </a>
                        </div>
                        <!-- end: .item -->
                        <!-- start: .item -->
                        <div class="item grid-33 tablet-grid-50 grid-parent">
                            <a href="oficinas/planta-33.php" class="fancybox" rel="oficinas">
                                <img src="images/oficinas-plantas/33.jpg">
                                <div class="bottom">
                                    <span class="mt"><strong>33</strong> m<sup>2</sup></span>
                                    <span class="cotizar">COTIZAR</span>
                                    <div class="clear"></div>
                                </div>
                            </a>
                        </div>
                        <!-- end: .item -->
                        <!-- start: .item -->
                        <div class="item grid-33 tablet-grid-50 grid-parent">
                            <a href="oficinas/planta-44.php" class="fancybox" rel="oficinas">
                                <img src="images/oficinas-plantas/44.jpg">
                                <div class="bottom">
                                    <span class="mt"><strong>44</strong> m<sup>2</sup></span>
                                    <span class="cotizar">COTIZAR</span>
                                    <div class="clear"></div>
                                </div>
                            </a>
                        </div>
                        <!-- end: .item -->
                        <!-- start: .item -->
                        <div class="item grid-33 tablet-grid-50 grid-parent">
                            <a href="oficinas/planta-58.php" class="fancybox" rel="oficinas">
                                <img src="images/oficinas-plantas/58.jpg">
                                <div class="bottom">
                                    <span class="mt"><strong>58</strong> m<sup>2</sup></span>
                                    <span class="cotizar">COTIZAR</span>
                                    <div class="clear"></div>
                                </div>
                            </a>
                        </div>
                        <!-- end: .item -->
                        <!-- start: .item -->
                        <div class="item grid-33 tablet-grid-50 grid-parent">
                            <a href="oficinas/planta-66.php" class="fancybox" rel="oficinas">
                                <img src="images/oficinas-plantas/66.jpg">
                                <div class="bottom">
                                    <span class="mt"><strong>66</strong> m<sup>2</sup></span>
                                    <span class="cotizar">COTIZAR</span>
                                    <div class="clear"></div>
                                </div>
                            </a>
                        </div>
                        <!-- end: .item -->
                        <!-- start: .item -->
                        <div class="item grid-33 tablet-grid-50 grid-parent">
                            <a href="oficinas/planta-75.php" class="fancybox" rel="oficinas">
                                <img src="images/oficinas-plantas/75.jpg">
                                <div class="bottom">
                                    <span class="mt"><strong>75</strong> m<sup>2</sup></span>
                                    <span class="cotizar">COTIZAR</span>
                                    <div class="clear"></div>
                                </div>
                            </a>
                        </div>
                        <!-- end: .item -->
                        <!-- start: .item -->
                        <div class="item grid-33 tablet-grid-50 grid-parent">
                            <a href="oficinas/planta-88.php" class="fancybox" rel="oficinas">
                                <img src="images/oficinas-plantas/88.jpg">
                                <div class="bottom">
                                    <span class="mt"><strong>88</strong> m<sup>2</sup></span>
                                    <span class="cotizar">COTIZAR</span>
                                    <div class="clear"></div>
                                </div>
                            </a>
                        </div>
                        <!-- end: .item -->
                    </div>
                </div>
            </section>
            <!-- end: #emplazamiento -->
            
            <!-- start: #emplazamiento -->
            <section id="emplazamiento" class="section grid-container">
            	<h2 class="titulo wow fadeInDown">EMPLAZAMIENTO</h2>
                <a href="images/oficinas-emplazamiento-hd.jpg" class="lightbox" data-fancybox-type="image"><img src="images/oficinas-emplazamiento.jpg" class="aligncenter wow fadeIn" data-wow-delay="500ms"></a>
            </section>
            <!-- end: #emplazamiento -->
            
            <?php include('templates/tour.php'); ?>
            
            <?php include('templates/ubicacion.php'); ?>
            
            <!-- start: #contacto -->
            <section id="contacto" class="section">
            	<div class="grid-container">
                    <h2 class="titulo wow fadeInDown">CONTÁCTANOS</h2>
                    <div class="info grid-35 tablet-grid-50 mobile-grid-100 wow fadeInLeft" data-wow-delay="500ms">
                        <img src="images/oficinas-contacto.jpg">
                        <div class="grid-50 tablet-grid-50 grid-parent">
                            <h4>REUNÁMONOS</h4>
                            <p>Av. Nueva Costanera 4040<br>Planta baja, Vitacura</p>
                        </div>
                        <div class="grid-50 tablet-grid-50 grid-parent right">
                            <h4>HABLEMOS</h4>
                            <p>+56 9 83572158<br>info@ivertical.cl</p>
                        </div>
                    </div>
                    <form action="send.php" method="post" data-url="enviar.php" class="formulario grid-55 tablet-grid-50 mobile-grid-100 wow fadeInRight" data-wow-delay="500ms">
                    	<h3>SELECCIONA LA OFICINA QUE BUSCAS</h3>
                        <div class="input-wrapper">
                            <input type="text" name="nombre" class="inputbox" placeholder="Nombre completo" required>
                        </div>
                        <div class="input-wrapper">
                            <input type="email" name="email" class="inputbox" placeholder="Email" required>
                        </div>
                        <?php
							$oficinas = array('22', '28', '32', '44', '47', '66', '81', '88');
						?>
                        <div class="input-wrapper">
                            <select name="planta">
                                <option>Selecciona Oficina</option>
                                <?php foreach($oficinas as $of): ?>
                                    <option value="Oficina <?php echo $of; ?>">Oficina <?php echo $of; ?> m&sup2;</option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="input-wrapper">
                            <textarea name="mensaje" placeholder="Mensaje"></textarea>
                        </div>
                        <div class="msg-exito">Tu mensaje ha sido enviado exitosamente.</div>
                        <input type="hidden" name="action" value="ajax_contact">
                        <input type="hidden" name="proyecto" value="Oficinas">
                        <input type="submit" value="ENVIAR" class="button submit">
                    </form>
                </div>
            </section>
            <!-- end: #contacto -->
            
        	<div class="clear"></div>
        </main>
        <!-- end: .content -->
		
        <footer id="footer" role="contentinfo">
        	<div class="grid-container">
            	<h3 class="wow fadeInDown">Oficinas Centro Andino es un consorcio de Vertical y Aconcagua.</h3>
                <ul>
                	<li class="wow fadeIn" data-wow-offset="0" data-wow-delay="500ms"><a href="#"><img src="images/logo-ivertical-int.png"></a></li>
                    <li class="wow fadeIn" data-wow-offset="0" data-wow-delay="500ms"><a href="#"><img src="images/logo-aconcagua-int.png"></a></li>
                </ul>
                <p>Todas las imágenes, ambientaciones y fotos fueron elaborados con fines ilustrativos, no constituyendo necesariamente una representación de la realidad, las dimensiones y superficies son aproximadas.</p>
            </div>
            <a href="#" id="back-to-top"></a>
        </footer>
        
    </div>
    <!-- end: #wrapper -->
    
    <!--[if !IE]><!-->
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript">
    $(window).load(function(){
        if($(document).width() > 980){
            wow = new WOW({
                offset: 150
            });
            wow.init();
        }else{
            $('.wow').removeClass('wow');	
        }
    });
    </script>
    <!--<![endif]-->
    <script type="text/javascript">
    $(window).load(function(){
        $('body').addClass('loaded');
    });
    </script>    
   
</body>
</html>