<div class="grid-container">
    <h2 class="titulo">EMPLAZAMIENTO LOCAL COMERCIAL <?php echo $m2; ?> M&sup2;</h2>
    <div class="img grid-50 tablet-grid-50 textcenter">
        <img src="../images/stripcenter-locales/<?php echo $id; ?>.jpg">
    </div>
    <div class="info grid-50 tablet-grid-50">
        <div class="grid-50">
            <h3 class="titulo">LOCAL <?php echo $id; ?></h3>
            <ul>
                <li><strong>Frente:</strong> <?php echo $m2_frente; ?> m&sup2;</li>
                <li><strong>Profundidad:</strong> <?php echo $m2_profundidad; ?> m&sup2;</li>
                <li><strong>TOTAL:</strong> <?php echo $m2; ?> m&sup2;</li>
            </ul>
            <!--<a href="../pdf/local-<?php echo $id; ?>.pdf" class="pdf" target="_blank">VER FICHA EN PDF</a>-->
        </div>
        <div class="grid-50">
            <img src="../images/stripcenter-locales/emplazamiento-<?php echo $id; ?>.png" class="emplazamiento">
        </div>
        <div class="clear"></div>
        <form action="send.php" data-url="../enviar.php" class="formulario" method="post">
            <div class="grid-50">
                <h3>CONSULTA ARRIENDO</h3>
                <div class="input-wrapper">
                    <input type="text" name="nombre" class="inputbox" placeholder="Nombre completo" required>
                </div>
                <div class="input-wrapper">
                    <input type="email" name="email" class="inputbox" placeholder="Email" required>
                </div>
            </div>
            <div class="grid-50">
            	<h3 class="hide-on-tablet hide-on-mobile">&nbsp;</h3>
                <div class="input-wrapper">
                    <textarea name="mensaje" placeholder="Mensaje"></textarea>
                </div>
                <input type="hidden" name="proyecto" value="Stripcenter">
                <input type="hidden" name="planta" value="Local <?php echo $id; ?>">
                <input type="submit" value="COTIZAR" class="button submit alignright">
                <input type="hidden" name="action" value="ajax_contact">
            </div>
            <div class="clear"></div>
            <div class="msg-exito">Tu mensaje ha sido enviado exitosamente.</div>
        </form>
    </div>
    <div class="clear"></div>
</div>