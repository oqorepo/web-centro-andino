<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<link href="../css/grids.css" rel="stylesheet" type="text/css">
<link href="../css/estilos.css" rel="stylesheet" type="text/css">
<link href="../css/responsive.css" rel="stylesheet" type="text/css">
<!--[if (lt IE 9) & (!IEMobile)]>
<link rel="stylesheet" href="css/grids-ie.css" />
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->
<script type="text/javascript" src="../js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="../js/jquery.debounce.js"></script>
<script type="text/javascript" src="../js/jquery.placeholder.min.js"></script>
<script type="text/javascript" src="../js/contacto.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	/* Formularios */
	
	$('input[placeholder], textarea[placeholder]').placeholder();
	
	$('form div.input-wrapper .inputbox').focus(function(){
		$('form div.input-wrapper').removeClass('focus');
		$(this).parent().addClass('focus');
	});
	
});
</script>