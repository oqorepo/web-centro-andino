<!DOCTYPE HTML>
<!--[if IE 7]><html lang="es" class="ie7 ie"><![endif]-->  
<!--[if IE 8]><html lang="es" class="ie8 ie"><![endif]-->  
<!--[if IE 9]><html lang="es" class="ie9 ie"><![endif]-->  
<!--[if !IE]><!--><html lang="es"><!--<![endif]-->  
<head>
<title>Centro Andino</title>
<?php include('templates/head.php'); ?>

</head>
<body>




<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69585118-1', 'auto');
  ga('send', 'pageview');

</script>


    
    <div id="loader">
        <div class="cssload-loader"></div>
    </div>
    <!-- start: #wrapper -->
    <div id="wrapper">
    
        
    	<!-- start: .content -->
		<main id="home" role="main">
        
        	<!-- start: #top -->
			<section id="top">
                <a href="tel:+56229519269" class="fono">+562 2951 9269</a>
                <img src="images/top.jpg" class="fondo hide-on-tablet hide-on-desktop">
                <!-- start: #masterslider -->
                <div id="masterslider" class="master-slider ms-skin-default">
                    <!-- start: .ms-slide -->
                    <div class="ms-slide">
                        <img src="images/blank.gif" data-src="images/home-slider/slide1.jpg">
                        <img src="images/blank.gif" data-src="images/logo-slider.png" class="ms-layer" data-offset-x="100" data-offset-y="100" data-origin="tl" data-type="image" data-effect="front(long)" data-duration="1500" data-delay="800" data-ease="easeOutExpo">
                        <div class="ms-layer ms-caption" data-offset-x="150" data-origin="ml" data-type="text" data-effect="left(long)" data-duration="1500" data-delay="1200" data-ease="easeOutExpo">
                        1.700 M2 DE <br>LOCALES COMERCIALES 
                        </div>
                    </div>
                    <!-- end: .ms-slide -->
                    <!-- start: .ms-slide -->
                    <div class="ms-slide">
                        <img src="images/blank.gif" data-src="images/home-slider/slide2.jpg">
                        <img src="images/blank.gif" data-src="images/logo-slider.png" class="ms-layer" data-offset-x="100" data-offset-y="100" data-origin="tl" data-type="image" data-effect="front(long)" data-duration="1500" data-delay="800" data-ease="easeOutExpo">
                        <div class="ms-layer ms-caption" data-offset-x="150" data-origin="ml" data-type="text" data-effect="left(long)" data-duration="1500" data-delay="1200" data-ease="easeOutExpo">
                            UBICACIÓN<br>INCOMPARABLE
                        </div>
                    </div>
                    <!-- end: .ms-slide -->
                     <!-- start: .ms-slide -->
                    <div class="ms-slide">
                        <img src="images/blank.gif" data-src="images/home-slider/slide6.jpg">
                        <img src="images/blank.gif" data-src="images/logo-slider.png" class="ms-layer" data-offset-x="100" data-offset-y="100" data-origin="tl" data-type="image" data-effect="front(long)" data-duration="1500" data-delay="800" data-ease="easeOutExpo">
                        <div class="ms-layer ms-caption" data-offset-x="150" data-origin="ml" data-type="text" data-effect="left(long)" data-duration="1500" data-delay="1200" data-ease="easeOutExpo">
                            SECTOR <br>100% RESIDENCIAL 
                        </div>
                    </div>
                    <!-- end: .ms-slide -->
                     <!-- start: .ms-slide -->
                    <div class="ms-slide">
                        <img src="images/blank.gif" data-src="images/home-slider/slide4.jpg">
                        <img src="images/blank.gif" data-src="images/logo-slider.png" class="ms-layer" data-offset-x="100" data-offset-y="100" data-origin="tl" data-type="image" data-effect="front(long)" data-duration="1500" data-delay="800" data-ease="easeOutExpo">
                        <div class="ms-layer ms-caption" data-offset-x="150" data-origin="ml" data-type="text" data-effect="left(long)" data-duration="1500" data-delay="1200" data-ease="easeOutExpo">
                            GRAN POTENCIAL <br>COMERCIAL
                        </div>
                    </div>
                    <!-- end: .ms-slide -->
                    <!-- start: .ms-slide -->
                    <div class="ms-slide">
                        <img src="images/blank.gif" data-src="images/home-slider/slide5.jpg">
                        <img src="images/blank.gif" data-src="images/logo-slider.png" class="ms-layer" data-offset-x="100" data-offset-y="100" data-origin="tl" data-type="image" data-effect="front(long)" data-duration="1500" data-delay="800" data-ease="easeOutExpo">
                        <div class="ms-layer ms-caption" data-offset-x="150" data-origin="ml" data-type="text" data-effect="left(long)" data-duration="1500" data-delay="1200" data-ease="easeOutExpo">
                            ZONA DE ALTO <br>CRECIMIENTO HABITACIONAL
                        </div>
                    </div>
                    <!-- end: .ms-slide -->
                </div>
                <!-- end: #masterslider -->
                <form action="send.php" method="post" data-url="enviar.php" class="formulario wow fadeInRight">
                	<h3>Solicitar información</h3>
                    <!-- <div class="input-wrapper">
                    	<label class="checkbox"><strong></strong></label>                    	
                    </div> -->
                    <div class="input-wrapper">
                    	<input type="text" name="nombre" class="inputbox" placeholder="Nombre completo" required>
                    </div>
                    <div class="input-wrapper">
                    	<input type="email" name="email" class="inputbox" placeholder="Email" required>
                    </div>
                    <div class="input-wrapper">
                    	<input type="text" name="empresa" class="inputbox" placeholder="Empresa" required>
                    </div>
                    <div class="input-wrapper">
                        <textarea name="mensaje" class="" placeholder="Mensaje" required></textarea>
                    </div>
                    <input type="hidden" name="action" value="ajax_contact">
                    <div class="msg-exito">Tu mensaje ha sido enviado exitosamente.</div>
                    <input type="submit" value="ENVIAR" class="button submit">
                </form>
            </section>
            <!-- end: #top -->
            
            <header id="header" role="banner">
            	<div class="grid-container">
                    <h1 class="logo"><a href="index.php"><img src="images/logo.png"></a></h1>
                    <nav>
                        <ul>
                            <li><a href="#proyecto">CENTRO ANDINO</a></li>
                            <li><a href="#emplazamiento">EMPLAZAMIENTO</a></li>
                            <li><a href="#entorno">ENTORNO</a></li>
                            <li><a href="#tour">TOUR 360</a></li>
                            <li><a href="#ubicacion">UBICACIÓN</a></li>
                            <!-- <li><a href="stripcenter.php">STRIP CENTER</a></li> -->
                        </ul>
                    </nav>
                    <div class="menu-mobile"></div>
                </div>
                <div class="clear"></div>
            </header>
            
            <!-- start: #proyecto -->
            <section id="proyecto" class="section">
            	<div class="grid-container">
                    <h2 class="titulo wow fadeInDown">COMERCIO <span>A PASOS DE TU HOGAR</span></h2>
                    
                    <div class="item grid-33 tablet-grid-33 wow fadeIn" data-wow-delay="500ms">
                        <div><img src="images/icon-bag.png" alt=""></div>
                        <h4>STRIP CENTER CENTRO ANDINO</h4>
                        <p>1.700 m<sup>2</sup> de locales comerciales más 1 supermercado Lider Express. Cuenta con más de 150 estacionamientos para autos y más de 300 para bicicletas</p>
                    </div>
                    <div class="item grid-33 tablet-grid-33 wow fadeIn" data-wow-delay="500ms">
                        <div><img src="images/icon-family.png" alt=""></div>
                        <h4>ZONA DE INFLUENCIA</h4>
                        <p>Alto crecimiento habitacional, compuesta por 63.122 habitantes distribuidos en 16.458 hogares, aproximadamente 10.000 alumnos distribuidos en 4 colegios a menos de 5 minutos.</p>
                        <!-- <p><a href="stripcenter.php" class="icon">Leer más <img src="images/icono-leermas.png" class="alignmiddle"></a></p> -->
                    </div>
                    <div class="item grid-33 tablet-grid-33 wow fadeIn" data-wow-delay="500ms">
                        <div><img src="images/icon-ub.png" alt=""></div>
                        <h4>UN ENTORNO INCOMPARABLE </h4>
                        <p>Ubicado en pleno barrio residencial de LOS TRAPENSES. Zona desabastecida comercialmente, centro comercial más cercano a más de 2 km.</p>
                        <!-- <p><a href="oficinas.php" class="icon">Leer más <img src="images/icono-leermas.png" class="alignmiddle"></a></p> -->
                    </div>
                </div>
                <div class="info">
                    <div class="img grid-60 tablet-grid-50"></div>
                    <div class="texto alignright grid-40 tablet-grid-50">
                    	<div class="wow fadeInRight" data-wow-delay="500ms">
                            <h2>CENTROANDINO<br><span>MÁS CERCA DE TODO</span></h2>
                            <p>La alta conectividad y facilidad de accesos, son pilares fundamentales en el desarrollo del gran proyecto Centro Andino, un nuevo polo de desarrollo comercial, nace en Los Trapenses.</p>
                            <p>El agrado de estar cerca de todo:<br>Colegios Monte Tabor y Santiago College.<br>Mall Paseo Los Trapenses.<br>Bancos.<br>Supermercados<br>Farmacias.</p>
                        </div>
                    </div>
                <div class="clear"></div>
                </div>
                
            </section>
            <!-- end: #proyecto -->
            
            <!-- start: #plantas -->
            <section id="plantas" class="section">
            	<div class="grid-container">
                    <h2 class="titulo wow fadeInDown">LOCALES COMERCIALES PARA ARRIENDO</h2>
                    <div class="wow fadeIn" data-wow-delay="500ms">
                            <h3 class="mapa--title"><a href="#">PRIMER PISO</a></h3>
                            <div class="panes">
                                <!-- start: .tab -->
                                <div class="tab">
                                    <img src="images/piso-1.jpg" usemap="#piso1" class="mapa">
                                    <map name="piso1">
                                        <area class="mapa--planta" href="locales/local-01-A.php" coords="138,173,180,233" shape="rect" data-fancybox-group="piso1">
                                        <area class="mapa--planta" href="locales/local-01-B.php" coords="180,272,140,244" shape="rect" data-fancybox-group="piso1">
                                        <area class="mapa--planta" href="locales/local-02.php" coords="94,276,181,380" shape="rect" data-fancybox-group="piso1">
                                        <area class="mapa--planta" href="locales/local-03.php" coords="205,287,281,367" shape="rect" data-fancybox-group="piso1">
                                        <area class="mapa--planta" href="locales/local-04.php" coords="283,289,339,330" shape="rect" data-fancybox-group="piso1">
                                        <area class="mapa--planta" href="locales/local-05.php" coords="340,289,394,331" shape="rect" data-fancybox-group="piso1">
                                        <area class="mapa--planta" href="locales/local-06.php" coords="396,289,456,331" shape="rect" data-fancybox-group="piso1">
                                        <area class="mapa--planta" href="locales/local-07.php" coords="283,333,339,367" shape="rect" data-fancybox-group="piso1">
                                        <area class="mapa--planta" href="locales/local-08.php" coords="340,332,395,367" shape="rect" data-fancybox-group="piso1">
                                        <area class="mapa--planta" href="locales/local-09.php" coords="397,333,455,366" shape="rect" data-fancybox-group="piso1">
                                        <area class="mapa--planta" href="locales/local-11.php" coords="480,275,554,369" shape="rect" data-fancybox-group="piso1">
                                        <area class="mapa--planta" href="locales/local-12.php" coords="481,247,555,274" shape="rect" data-fancybox-group="piso1">
                                        <area class="mapa--planta" href="locales/local-13.php" coords="481,217,554,245" shape="rect" data-fancybox-group="piso1">
                                        <area class="mapa--planta" href="locales/local-14.php" coords="480,189,552,216" shape="rect" data-fancybox-group="piso1">
                                        <area class="mapa--planta" href="locales/local-15.php" coords="480,160,553,187" shape="rect data-fancybox-group="piso1"">
                                        <area class="mapa--planta" href="locales/local-16.php" coords="554,66,484,96,483,158,554,157" shape="poly" data-fancybox-group="piso1">
                                    </map>
                                </div>
                                <!-- start: .tab -->
                                <!-- start: .tab -->
                            <h3 class="mapa--title"><a href="#">SEGUNDO PISO</a></h3>
                            <div class="tab">
                                <img src="images/piso-2.jpg" usemap="#piso2" class="mapa">
                                <map name="piso2">
                                    <area class="mapa--planta" href="locales/local-17.php" coords="103,218,176,275" shape="rect" data-fancybox-group="piso2">
                                    <area class="mapa--planta" href="locales/local-18.php" coords="101,278,178,335" shape="rect" data-fancybox-group="piso2">
                                    <area class="mapa--planta" href="locales/local-19.php" coords="243,293,243,383,341,382,339,338,283,336,282,293" shape="poly" data-fancybox-group="piso2">
                                    <area class="mapa--planta" href="locales/local-20.php" coords="284,294,342,337" shape="rect" data-fancybox-group="piso2">
                                    <area class="mapa--planta" href="locales/local-21.php" coords="343,292,400,338" shape="rect" data-fancybox-group="piso2">
                                    <area class="mapa--planta" href="locales/local-22.php" coords="342,339,343,382,428,385,428,293,403,293,402,336" shape="poly" data-fancybox-group="piso2">
                                    <area class="mapa--planta" href="locales/local-23.php" coords="429,293,459,293,458,350,474,352,476,384,429,382" shape="poly" data-fancybox-group="piso2">
                                    <area class="mapa--planta" href="locales/local-24.php" coords="486,279,556,279,558,371,525,369,523,315,485,315,486,299" shape="poly" data-fancybox-group="piso2">
                                    <area class="mapa--planta" href="locales/local-25.php" coords="486,250,555,276" shape="rect" data-fancybox-group="piso2">
                                    <area class="mapa--planta" href="locales/local-26.php" coords="487,222,558,249" shape="rect" data-fancybox-group="piso2">
                                    <area class="mapa--planta" href="locales/local-27.php" coords="487,140,557,221" shape="rect" data-fancybox-group="piso2">
                                </map>
                            </div>
                            <!-- start: .tab -->
                        </div>
                    </div>
                </div>
            </section>
            <!-- end: #plantas -->
            <!-- start: #servicios -->
            <section id="servicios">
            	<div class="grid-container overflow">
                    <div class="item grid-25 tablet-grid-25 mobile-grid-50 wow fadeInUp">
                        <img src="images/icono-colegios.png">
                        COLEGIOS
                    </div>
                    <div class="item grid-25 tablet-grid-25 mobile-grid-50 wow fadeInUp" data-wow-delay="400ms">
                        <img src="images/icono-paisajismo.png">
                        PAISAJISMO
                    </div>
                    <div class="item grid-25 tablet-grid-25 mobile-grid-50 wow fadeInUp" data-wow-delay="800ms">
                        <img src="images/icono-conectividad.png">
                        CONECTIVIDAD
                    </div>
                    <div class="item grid-25 tablet-grid-25 mobile-grid-50 wow fadeInUp" data-wow-delay="1200ms">
                        <img src="images/icono-malls.png">
                        SHOPPING
                    </div>
                </div>
            </section>
            <!-- end: #servicios -->
            
            <!-- start: #entorno -->
            <section id="entorno" class="galeria section">
            	<div class="grid-container">
                    <h2 class="titulo wow fadeInDown">ENTORNO</h2>
                    <h3 class="bajada wow fadeInDown">Vistas despejadas y en el mejor sector de Los Trapenses</h3>
                    <div class="wow fadeIn" data-wow-delay="500ms">
                        <ul class="tabs">
                            <li><a href="#">IMÁGENES</a></li>
                            <li><a href="#">VIDEO</a></li>
                        </ul>
                        <div class="panes">
                            <!-- start: .tab -->
                            <div class="tab">
                                <div class="slick-slider">
                                    <div class="item"><img src="images/home-entorno/1.jpg"></div>
                                    <div class="item"><img src="images/home-entorno/2.jpg"></div>
                                    <div class="item"><img src="images/home-entorno/3.jpg"></div>
                                    <div class="item"><img src="images/home-entorno/4.jpg"></div>
                                    <div class="item"><img src="images/home-entorno/5.jpg"></div>
                                </div>
                            </div>
                            <!-- end: .tab -->
                            <!-- start: .tab -->
                            <div class="tab">
                                <iframe width="100%" height="477" src="https://www.youtube.com/embed/HTpjzPqASfA?rel=0&showinfo=0&vq=large" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <!-- end: .tab -->
                        </div>
                    </div>
                </div>
            </section>
            <!-- end: #entorno -->
            
            <?php include('templates/tour.php'); ?>
            
            <?php include('templates/ubicacion.php'); ?>

            <!-- start: #contacto -->
            <section id="contacto" class="section">
                <div class="grid-container">
                    <h2 class="titulo wow fadeInDown">CONSULTA POR NUESTROS LOCALES DISPONIBLES</h2>
                    <div class="info grid-35 tablet-grid-50 mobile-grid-100 wow fadeInLeft" data-wow-delay="500ms">
                        <img src="images/stripcenter-contacto.jpg">
                        <div class="grid-50 tablet-grid-50 grid-parent">
                            <h4>REUNÁMONOS</h4>
                            <p>Av. Nueva Costanera 4040<br>Planta baja, Vitacura</p>
                        </div>
                        <div class="grid-50 tablet-grid-50 grid-parent right">
                            <h4>HABLEMOS</h4>
                            <p>Celular +56 9 61492952 <br> Oficina +56 2 29519269 <br> catalina.hurtado@ifbinversiones.cl</p>
                        </div>
                    </div>
                    <form action="send.php" method="post" data-url="enviar.php" class="formulario grid-55 tablet-grid-50 mobile-grid-100 wow fadeInRight" data-wow-delay="500ms">
                        <h3>ESCOGE EL LOCAL QUE QUIERES ARRENDAR</h3>
                        <div class="input-wrapper">
                            <input type="text" name="nombre" class="inputbox" placeholder="Nombre completo" required>
                        </div>
                        <div class="input-wrapper">
                            <input type="email" name="email" class="inputbox" placeholder="Email" required>
                        </div>
                        <div class="input-wrapper">
                            <select name="local" required>
                                <option>Consultas generales...</option>
                                <option value="Local 1 A">Local 1 A</option>
                                <option value="Local 1 B">Local 1 B</option>
                                <?php for($i = 2; $i < 28; $i++): ?>
                                    <option value="Local <?php echo $i; ?>">Local <?php echo $i; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="input-wrapper">
                            <textarea name="mensaje" placeholder="Mensaje"></textarea>
                        </div>
                        <div class="msg-exito">Tu mensaje ha sido enviado exitosamente.</div>
                        <input type="hidden" name="action" value="ajax_contact">
                        <input type="hidden" name="proyecto" value="Stripcenter">
                        <input type="submit" value="ENVIAR" class="button submit">
                    </form>
                </div>
            </section>
            <!-- end: #contacto -->
            
        	<div class="clear"></div>
        </main>
        <!-- end: .content -->
		
        <footer id="footer" role="contentinfo">
        	<div class="grid-container">
            	<h3 class="wow fadeInDown">Centro Andino es un proyecto de</h3>
                <ul>
                	<li class="wow fadeIn" data-wow-offset="0" data-wow-delay="500ms">
                        <a href=""><img src="images/logo_angularv2.png"></a>
                    </li>
                    <li class="wow fadeIn" data-wow-offset="0" data-wow-delay="500ms">
                        <a href=""><img src="images/logo-ifbv2.png"></a>
                    </li>
                    <li class="wow fadeIn" data-wow-offset="0" data-wow-delay="500ms">
                        <a href=""><img src="images/logo_mascenterv2.png"></a>
                    </li>
                </ul>
                <h4>Avenida Pie Andino 5.801, Los Trapenses.</h4>
                <p>Todas las imágenes, ambientaciones y fotos fueron elaborados con fines ilustrativos, no constituyendo necesariamente una representación de la realidad, las dimensiones y superficies son aproximadas.</p>
            </div>
            <a href="#" id="back-to-top"></a>
        </footer>
        
    </div>
    <!-- end: #wrapper -->
    
    <!--[if !IE]><!-->
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript">
    $(window).load(function(){
        if($(document).width() > 980){
            wow = new WOW({
                offset: 150
            });
            wow.init();
        }else{
            $('.wow').removeClass('wow');	
        }
    });
    </script>
    <!--<![endif]-->
    <script type="text/javascript">
    $(window).load(function(){
        $('body').addClass('loaded');
    });
    $(document).ready(function() {
        $('map').imageMapResize();
    });
    </script>    
   
</body>
</html>