<div class="grid-container">
    <h2 class="titulo">OFICINAS HABILITADAS DESDE <?php echo $m2; ?> M&sup2;</h2>
    <div class="img grid-50 tablet-grid-50 textcenter">
        <img src="../images/oficinas-plantas/<?php echo $id; ?>-hd.jpg">
    </div>
    <div class="info grid-50 tablet-grid-50">
        <h3 class="titulo"><?php echo $m2; ?> M&sup2;</h3>
        <!--<a href="../pdf/planta-<?php echo $id; ?>.pdf" class="pdf" target="_blank">VER FICHA EN PDF</a>-->
        <img src="../images/oficinas-plantas/emplazamiento-<?php echo $id; ?>.jpg" class="emplazamiento">
        
        <form action="send.php" data-url="../enviar.php" class="formulario" method="post">
            <div class="grid-50">
                <h3>COTIZA ESTA OFICINA</h3>
                <div class="input-wrapper">
                    <input type="text" name="nombre" class="inputbox" placeholder="Nombre completo" required>
                </div>
                <div class="input-wrapper">
                    <input type="email" name="email" class="inputbox" placeholder="Email" required>
                </div>
            </div>
            <div class="grid-50">
            	<h3 class="hide-on-tablet hide-on-mobile">&nbsp;</h3>
                <div class="input-wrapper">
                    <textarea name="mensaje" placeholder="Mensaje"></textarea>
                </div>
                <input type="hidden" name="proyecto" value="Oficinas">
                <input type="hidden" name="planta" value="Oficina <?php echo $m2; ?>m2">
                <input type="submit" value="COTIZAR" class="button submit alignright">
                <input type="hidden" name="action" value="ajax_contact">
            </div>
            <div class="clear"></div>
            <div class="msg-exito">Tu mensaje ha sido enviado exitosamente.</div>
        </form>
    </div>
    <div class="clear"></div>
</div>