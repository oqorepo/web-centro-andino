<!-- start: #ubicacion -->
<section id="ubicacion" class="section">
    <h2 class="titulo wow fadeInDown">UBICACIÓN</h2>
    <h3 class="bajada wow fadeInDown">Avenida Pie Andino 5.801, Los Trapenses</h3>
    <div class="wow fadeIn" data-wow-delay="500ms">
        <ul class="tabs">
            <li><a href="#">MAPA GOOGLE</a></li>
            <li><a href="#">MAPA ILUSTRADO</a></li>
        </ul>
        <div class="panes">
            <!-- start: .tab -->
            <div class="tab">
           
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5606.803714048137!2d-70.54110399213157!3d-33.32337892662317!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzPCsDE5JzIyLjYiUyA3MMKwMzInMTYuMiJX!5e0!3m2!1ses!2scl!4v1522784782496" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

                <div id="options" style="display: none;" data-lat="-33.3241375" data-lng="-70.5375354" data-zoom="15" data-marker="images/marker.png">
                   <p><img src="images/tip-gmap.png"></p>
                </div>
            </div>
            <!-- end: .tab -->
            <!-- start: .tab -->
            <div class="tab">
                <img src="images/mapa-ilustrado.jpg" class="aligncenter">
            </div>
            <!-- end: .tab -->
        </div>
    </div>
</section>
<!-- end: #ubicacion -->