<!-- start: #header -->
<header id="header" role="banner">
    <a href="#" class="volver">< Volver a mejora tu pensión</a>
    <h1 class="logo"><a href="index.php"><img src="images/logo.png"></a></h1>
    <nav>
        <ul>
            <li>
                <a href="#login" class="icono fancybox underline ingreso" data-fancybox-type="inline">Ingresar al Concurso</a>
                <div class="clear"></div>
                <a href="#recuperar-pass" class="secundario fancybox" data-fancybox-type="inline">Recuperar Contraseña</a>
            </li>
            <li>
                <a href="#crear-cuenta" class="icono underline fancybox crear" data-fancybox-type="inline">Crear Nueva Cuenta</a>
            </li>
        </ul>
    </nav>
    <div class="clear"></div>
</header>
<!-- end: #header -->     