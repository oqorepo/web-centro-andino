<!-- start: #tour -->
<section id="tour" class="section">
    <div class="grid-container">
        <h2 class="titulo wow fadeInDown">VISTA 360°</h2>
        <h3 class="bajada wow fadeInDown">El agrado de estar cerca de todo</h3>
        <div class="wow fadeIn" data-wow-delay="500ms">
            <iframe src="http://lanube360.com/oqo/centro-andino/" width="960" height="470" frameborder="0" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" oallowfullscreen="true" msallowfullscreen="true"></iframe>
        </div>
    </div>
</section>
<!-- end: #tour -->