<?php
//Rellenar con los datos de la planta:
// El ID es recomendable que sea un valor similar a los m2 solo que no puede ser con comas.

$id = 19; //solo numeros enteros y no se puede repetir, se utilizara para saber el nombre del archivo PDF, la imagen de emplazamiento y la imagen de la planta.
$m2 = '128,5'; // Sera utilizado para mostrar la cantidad de metros cuadrados (es solo informativo)
$m2_frente = '11,67';
$m2_profundidad = '10,60';

?>
<!DOCTYPE HTML>
<!--[if IE 7]><html lang="es" class="ie7 ie"><![endif]-->  
<!--[if IE 8]><html lang="es" class="ie8 ie"><![endif]-->  
<!--[if IE 9]><html lang="es" class="ie9 ie"><![endif]-->  
<!--[if !IE]><!--><html lang="es"><!--<![endif]-->  
<head>
<title>Centro Andino - Local <?php echo $id; ?> m2</title>
<?php include('head.php'); ?>
</head>
<body class="light">
    <!-- start: #wrapper -->
    <div id="wrapper">
    
    	<!-- start: .content -->
		<main id="lightbox" class="stripcenter" role="main">
        	<?php include('content.php'); ?>

        	<div class="clear"></div>
        </main>
        <!-- end: .content -->
        
    </div>
    <!-- end: #wrapper -->
</body>
</html>