<!DOCTYPE HTML>
<!--[if IE 7]><html lang="es" class="ie7 ie"><![endif]-->  
<!--[if IE 8]><html lang="es" class="ie8 ie"><![endif]-->  
<!--[if IE 9]><html lang="es" class="ie9 ie"><![endif]-->  
<!--[if !IE]><!--><html lang="es"><!--<![endif]-->  
<head>
<title>Centro Andino - Stripcenter</title>
<?php include('templates/head.php'); ?>
<script type="text/javascript" src="js/jquery.imagemapster.min.js"></script>
<script type="text/javascript">
$(window).load(function(){
	
	$('#masterslider').masterslider({
		width: $(window).width(),
		height: ($(window).height()-77),
		space: 0,
		speed: 20,
		instantStartLayers: true,
		layout: 'fullwidth',
		loop: true,
		preload: 0,
		autoplay: true,
		view: 'basic',
		controls : {
			arrows : { autohide: false }
		}
	});
	
	$('div.slick-slider').slick({
		cssEase: 'ease',
		fade: false,
		arrows: true,
		infinite: true,
		dots: false,
		autoplay: true,
		autoplaySpeed: 4000,
		pauseOnHover: false,
		slidesToShow: 1,
		slidesToScroll: 1
	});
	
	$('img.mapa').mapster({
		showToolTip: true,
		clickNavigate: false,
		isSelectable: false,
		fillColor: 'f47300',
		fillOpacity: 0.6,
		stroke: false,
	});
	
	$('#plantas ul.tabs').tabs('div.panes div.tab', {
		history: false,
		effect: 'fade'
	});
	
	
});
</script>
</head>
<body class="stripcenter">
    

    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69585118-1', 'auto');
  ga('send', 'pageview');

</script>


	<div id="loader">
        <div class="cssload-loader"></div>
    </div>
    <!-- start: #wrapper -->
    <div id="wrapper">
        
    	<!-- start: .content -->
		<main id="proyecto-int" class="stripcenter" role="main">
        
        	<!-- start: #top -->
			<section id="top">
                <a href="tel:+56226118400" class="fono">+562 2951 9269 <span>CONVERSEMOS</span></a>
                <!-- start: #masterslider -->
                <div id="masterslider" class="master-slider ms-skin-default">
                    <!-- start: .ms-slide -->
                    <div class="ms-slide">
                        <img src="images/blank.gif" data-src="images/stripcenter-slider/1.jpg">
                        <div class="ms-layer ms-caption" style="left:45px; bottom:60px;" data-type="text" data-effect="left(500)"  data-duration="1500" data-delay="900" data-ease="easeOutExpo">
                            <strong>ARRIENDO DE LOCALES COMERCIALES</strong><br>
                            EN LOS TRAPENSES
                        </div>
                    </div>
                    <!-- end: .ms-slide -->
                    <!-- start: .ms-slide -->
                    <div class="ms-slide">
                        <img src="images/blank.gif" data-src="images/stripcenter-slider/2.jpg">
                        <div class="ms-layer ms-caption" style="left:45px; bottom:60px;" data-type="text" data-effect="left(500)"  data-duration="1500" data-delay="900" data-ease="easeOutExpo">
                            ARRIENDO DE LOCALES COMERCIALES<br>
                            <strong>EN LOS TRAPENSES</strong>
                        </div>
                    </div>
                    <!-- end: .ms-slide -->
                    <!-- start: .ms-slide -->
                    <div class="ms-slide">
                        <img src="images/blank.gif" data-src="images/stripcenter-slider/3.jpg">
                        <div class="ms-layer ms-caption" style="left:45px; bottom:60px;" data-type="text" data-effect="left(500)"  data-duration="1500" data-delay="900" data-ease="easeOutExpo">
                            <strong>ARRIENDO DE LOCALES COMERCIALES</strong><br>
                            EN LOS TRAPENSES
                        </div>
                    </div>
                    <!-- end: .ms-slide -->
                </div>
                <!-- end: #masterslider -->
            </section>
            <!-- end: #top -->
            
            <!-- start: #header -->
            <header id="header" role="banner">
            	<div class="grid-container">
                    <h1 class="logo"><a href="index.php"><img src="images/logo.png"></a></h1>
                    <nav>
                        <ul>
                            <li><a href="index.php#proyecto">CENTRO ANDINO</a></li>
                            <li><a href="index.php#emplazamiento">EMPLAZAMIENTO</a></li>
                            <li><a href="index.php#entorno">ENTORNO</a></li>
                            <li><a href="index.php#tour">TOUR 360</a></li>
                            <li><a href="index.php#ubicacion">UBICACIÓN</a></li>
                            <li><a href="stripcenter.php" class="active-stripcenter">STRIP CENTER</a></li>
                        </ul>
                    </nav>
                    <div class="menu-mobile"></div>
                </div>
                <div class="clear"></div>
                <div class="submenu hide-on-tablet hide-on-mobile">
                	<div class="grid-container">
                        <ul>
                            <li><a href="#descripcion">DESCRIPCIÓN</a></li>
                            <li><a href="#galeria">GALERÍA</a></li>
                            <li><a href="#plantas">MODELOS</a></li>
                            <li><a href="#contacto">CONTACTO</a></li>
                        </ul>
                    </div>
                </div>
            </header>
            <!-- end: #header -->
            
            <!-- start: #descripcion -->
            <section id="descripcion" class="section">
            	<div class="grid-container">
                	<h2 class="titulo wow fadeInDown">STRIPCENTER CENTRO ANDINO</h2>
               		<div class="grid-60 tablet-grid-100 wow fadeInLeft" data-wow-delay="500ms">
                    	<img src="images/stripcenter-descripcion.jpg" class="fullwidth">
                    </div>
                    <div class="grid-30 tablet-grid-100  alignright wow fadeInRight" data-wow-delay="500ms">
                    	<div class="texto">
                        	<h5 class="light">1.700 m<sup>2</sup> DE LOCALES<br>COMERCIALES</h5>
                            <p>Ubicado en pleno centro residencial de Los Trapenses y emplazado sobre la futura circunvalación de Lo Barnechea se encuentra el Stripcenter Centro Andino, con 26 locales comerciales en 1700 m<sup>2</sup> construidos , mas un supermercado subterraneo y amplios estacionamientos.</p>
                        </div>
                    </div>
                </div>
                
            </section>
            <!-- end: #descripcion -->
            
            <!-- start: #galeria -->
            <section id="galeria" class="galeria section">
            	<div class="grid-container">
                    <h2 class="titulo wow fadeInDown">GALERÍA</h2>
                    <div class="wow fadeIn" data-wow-delay="500ms">
                        <ul class="tabs">
                            <li><a href="#">IMÁGENES</a></li>
                            <li><a href="#">VIDEO</a></li>
                        </ul>
                        <div class="panes">
                            <!-- start: .tab -->
                            <div class="tab">
                                <div class="slick-slider">
                                    <div class="item"><a href="images/stripcenter-galeria/1-hd.jpg" class="lightbox" rel="galeria"><img src="images/stripcenter-galeria/1.jpg"></a></div>
                                    <div class="item"><a href="images/stripcenter-galeria/2-hd.jpg" class="lightbox" rel="galeria"><img src="images/stripcenter-galeria/2.jpg"></a></div>
                                    <div class="item"><a href="images/stripcenter-galeria/3-hd.jpg" class="lightbox" rel="galeria"><img src="images/stripcenter-galeria/3.jpg"></a></div>
                                    <div class="item"><a href="images/stripcenter-galeria/4-hd.jpg" class="lightbox" rel="galeria"><img src="images/stripcenter-galeria/4.jpg"></a></div>
                                    <div class="item"><a href="images/stripcenter-galeria/5-hd.jpg" class="lightbox" rel="galeria"><img src="images/stripcenter-galeria/5.jpg"></a></div>
                                    <div class="item"><a href="images/stripcenter-galeria/6-hd.jpg" class="lightbox" rel="galeria"><img src="images/stripcenter-galeria/6.jpg"></a></div>
                                </div>
                            </div>
                            <!-- end: .tab -->
                            <!-- start: .tab -->
                            <div class="tab">
                                <iframe width="100%" height="477" src="https://www.youtube.com/embed/HTpjzPqASfA?rel=0&showinfo=0&vq=large" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <!-- end: .tab -->
                        </div>
                    </div>
                </div>
            </section>
            <!-- end: #galeria -->
            
            <!-- start: #plantas -->
            <section id="plantas" class="section">
            	<div class="grid-container">
                    <h2 class="titulo wow fadeInDown">EMPLAZAMIENTO LOCALES COMERCIALES PARA ARRIENDO</h2>
                    <div class="wow fadeIn" data-wow-delay="500ms">
                        <ul class="tabs noplugin">
                            <li><a href="#">PRIMER PISO</a></li>
                            <li><a href="#">SEGUNDO PISO</a></li>
                        </ul>
                        <div class="panes">
                            <!-- start: .tab -->
                            <div class="tab">
                                <img src="images/stripcenter-piso1.png" usemap="#piso1" class="mapa">
                                <map name="piso1">
                                	<area shape="rect" coords="44,106,72,161" href="locales/local-01-A.php" data-fancybox-group="piso1">
                                  	<area shape="rect" coords="44,172,72,201" href="locales/local-01-B.php" data-fancybox-group="piso1">
                                    <area shape="rect" coords="2,202,73,304" href="locales/local-02.php" data-fancybox-group="piso1">
                                    <area shape="rect" coords="103,216,177,290" href="locales/local-03.php" data-fancybox-group="piso1">
                                    <area shape="rect" coords="177,217,234,254" href="locales/local-04.php" data-fancybox-group="piso1">
                                    <area shape="rect" coords="235,217,287,254" href="locales/local-05.php" data-fancybox-group="piso1">
                                    <area shape="rect" coords="288,217,343,254" href="locales/local-06.php" data-fancybox-group="piso1">
                                    <area shape="rect" coords="179,256,234,291" href="locales/local-07.php" data-fancybox-group="piso1">
                                    <area shape="rect" coords="235,256,286,290" href="locales/local-08.php" data-fancybox-group="piso1">
                                    <area shape="rect" coords="288,255,343,290" href="locales/local-09.php" data-fancybox-group="piso1">
                                    <area shape="rect" coords="371,204,441,290" href="locales/local-11.php" data-fancybox-group="piso1">
                                    <area shape="rect" coords="371,176,441,203" href="locales/local-12.php" data-fancybox-group="piso1">
                                    <area shape="rect" coords="370,149,440,176" href="locales/local-13.php" data-fancybox-group="piso1">
                                    <area shape="rect" coords="370,120,440,147" href="locales/local-14.php" data-fancybox-group="piso1">
                                	<area shape="rect" coords="370,92,440,119" href="locales/local-15.php" data-fancybox-group="piso1">
                                    <area shape="rect" coords="370,27,441,91" href="locales/local-16.php" data-fancybox-group="piso1">
                                </map>
                            </div>
                            <!-- start: .tab -->
                            <!-- start: .tab -->
                            <div class="tab">
                                <img src="images/stripcenter-piso2.png" usemap="#piso2" class="mapa">
                                <map name="piso2">
                                	<area shape="rect" coords="3,101,90,170" href="locales/local-17.php" data-fancybox-group="piso2">
                                    <area shape="rect" coords="3,172,90,241" href="locales/local-18.php" data-fancybox-group="piso2">
                                    <area shape="poly" coords="293,244,293,298,173,298,171,190,220,190,221,244" href="locales/local-19.php" data-fancybox-group="piso2">
                                    <area shape="rect" coords="221,189,292,242" href="locales/local-20.php" data-fancybox-group="piso2">
                                    <area shape="rect" coords="293,190,362,242" href="locales/local-21.php" data-fancybox-group="piso2">
                                    <area shape="poly" coords="364,190,396,190,396,297,292,297,293,244,364,244" href="locales/local-22.php" data-fancybox-group="piso2">
                                    <area shape="poly" coords="396,189,430,189,430,259,451,260,450,297,396,297" href="locales/local-23.php" data-fancybox-group="piso2">
                                    <area shape="poly" coords="465,171,551,171,552,283,509,284,508,215,466,215" href="locales/local-24.php" data-fancybox-group="piso2">
                                    <area shape="rect" coords="466,138,551,169" href="locales/local-25.php" data-fancybox-group="piso2">
                                	<area shape="rect" coords="465,104,551,136" href="locales/local-26.php" data-fancybox-group="piso2">
                                    <area shape="rect" coords="466,3,552,103" href="locales/local-27.php" data-fancybox-group="piso2">
                                </map>
                            </div>
                            <!-- start: .tab -->
                        </div>
                    </div>
                </div>
            </section>
            <!-- end: #emplazamiento -->
            
            <!-- start: #emplazamiento -->
            <section id="emplazamiento" class="section grid-container">
            	<h2 class="titulo wow fadeInDown">EMPLAZAMIENTO</h2>
                <a href="images/stripcenter-emplazamiento-hd.jpg" class="lightbox" data-fancybox-type="image"><img src="images/stripcenter-emplazamiento.jpg" class="aligncenter wow fadeIn" data-wow-delay="500ms"></a>
            </section>
            <!-- end: #emplazamiento -->
            
            <?php include('templates/tour.php'); ?>
            
            <?php include('templates/ubicacion.php'); ?>
            
            <!-- start: #contacto -->
            <section id="contacto" class="section">
            	<div class="grid-container">
                    <h2 class="titulo wow fadeInDown">CONSULTA POR NUESTROS LOCALES COMERCIALES</h2>
                    <div class="info grid-35 tablet-grid-50 mobile-grid-100 wow fadeInLeft" data-wow-delay="500ms">
                        <img src="images/stripcenter-contacto.jpg">
                        <div class="grid-50 tablet-grid-50 grid-parent">
                            <h4>REUNÁMONOS</h4>
                            <p>Av. Nueva Costanera 4040<br>Planta baja, Vitacura</p>
                        </div>
                        <div class="grid-50 tablet-grid-50 grid-parent right">
                            <h4>HABLEMOS</h4>
                            <p>Celular +56 9 61492952 <br> Oficina +56 2 29519269 <br> catalina.hurtado@ifbinversiones.cl</p>
                        </div>
                    </div>
                    <form action="send.php" method="post" data-url="enviar.php" class="formulario grid-55 tablet-grid-50 mobile-grid-100 wow fadeInRight" data-wow-delay="500ms">
                    	<h3>ESCOGE EL LOCAL QUE QUIERES ARRENDAR</h3>
                        <div class="input-wrapper">
                            <input type="text" name="nombre" class="inputbox" placeholder="Nombre completo" required>
                        </div>
                        <div class="input-wrapper">
                            <input type="email" name="email" class="inputbox" placeholder="Email" required>
                        </div>
                        <div class="input-wrapper">
                            <select name="local" required>
                                <option>Consultas generales...</option>
                                <option value="Local 1 A">Local 1 A</option>
                                <option value="Local 1 B">Local 1 B</option>
                                <?php for($i = 2; $i < 28; $i++): ?>
                                    <option value="Local <?php echo $i; ?>">Local <?php echo $i; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="input-wrapper">
                            <textarea name="mensaje" placeholder="Mensaje"></textarea>
                        </div>
                        <div class="msg-exito">Tu mensaje ha sido enviado exitosamente.</div>
                        <input type="hidden" name="action" value="ajax_contact">
                        <input type="hidden" name="proyecto" value="Stripcenter">
                        <input type="submit" value="ENVIAR" class="button submit">
                    </form>
                </div>
            </section>
            <!-- end: #contacto -->
            
        	<div class="clear"></div>
        </main>
        <!-- end: .content -->
		
        <footer id="footer" class="stripcenter" role="contentinfo">
        	<div class="grid-container">
            	<h3 class="wow fadeInDown">Stripcenter Centro Andino es un consorcio de Activos y Rentas con IFB Inversuones.</h3>
                <ul>
                	<li class="wow fadeIn" data-wow-offset="0" data-wow-delay="500ms"><a href="#"><img src="images/logo-activos-rentas-int.png"></a></li>
                    <li class="wow fadeIn" data-wow-offset="0" data-wow-delay="500ms"><a href="#"><img src="images/logo-ifb-int.png"></a></li>
                </ul>
                <p>Todas las imágenes, ambientaciones y fotos fueron elaborados con fines ilustrativos, no constituyendo necesariamente una representación de la realidad, las dimensiones y superficies son aproximadas.</p>
            </div>
            <a href="#" id="back-to-top"></a>
        </footer>
        
    </div>
    <!-- end: #wrapper -->
    
    <!--[if !IE]><!-->
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript">
    $(window).load(function(){
        if($(document).width() > 980){
            wow = new WOW({
                offset: 150
            });
            wow.init();
        }else{
            $('.wow').removeClass('wow');	
        }
    });
    </script>
    <!--<![endif]-->
    <script type="text/javascript">
    $(window).load(function(){
        $('body').addClass('loaded');
    });
    </script>    
   
</body>
</html>