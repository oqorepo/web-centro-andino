<!DOCTYPE HTML>
<!--[if IE 7]><html lang="es" class="ie7 ie"><![endif]-->  
<!--[if IE 8]><html lang="es" class="ie8 ie"><![endif]-->  
<!--[if IE 9]><html lang="es" class="ie9 ie"><![endif]-->  
<!--[if !IE]><!--><html lang="es"><!--<![endif]-->  
<head>
<title>Centro Andino</title>
<?php include('templates/head.php'); ?>
<script type="text/javascript">
$(window).load(function(){
	
	$('#masterslider').masterslider({
		width: $(window).width(),
		height: ($(window).height()-77),
		space: 0,
		speed: 20,
		instantStartLayers: true,
		layout: 'fullwidth',
		loop: true,
		preload: 0,
		autoplay: true,
		view: 'basic',
/*		controls : {
			arrows : { autohide: false }
		}*/
	});
	
	$('div.slick-slider').slick({
		cssEase: 'ease',
		fade: false,
		arrows: true,
		infinite: true,
		dots: false,
		autoplay: true,
		autoplaySpeed: 4000,
		pauseOnHover: false,
		slidesToShow: 1,
		slidesToScroll: 1
	});
	
});
</script>
</head>
<body>




<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69585118-1', 'auto');
  ga('send', 'pageview');

</script>


    
    <div id="loader">
        <div class="cssload-loader"></div>
    </div>
    <!-- start: #wrapper -->
    <div id="wrapper">
    
        
    	<!-- start: .content -->
		<main id="home" role="main">
        
        	<!-- start: #top -->
			<section id="top">
                <a href="tel:+56226118400" class="fono">+562 2951 9269 <span>CONVERSEMOS</span></a>
                <img src="images/top.jpg" class="fondo hide-on-tablet hide-on-desktop">
                <!-- start: #masterslider -->
                <div id="masterslider" class="master-slider ms-skin-default">
                    <!-- start: .ms-slide -->
                    <div class="ms-slide">
                        <img src="images/blank.gif" data-src="images/home-slider/1.jpg">
                        <img src="images/blank.gif" data-src="images/logo-slider.png" class="ms-layer" data-offset-x="100" data-offset-y="100" data-origin="tl" data-type="image" data-effect="front(long)" data-duration="1500" data-delay="800" data-ease="easeOutExpo">
                        <div class="ms-layer ms-caption" data-offset-x="150" data-origin="ml" data-type="text" data-effect="left(long)" data-duration="1500" data-delay="1200" data-ease="easeOutExpo">
                            OFICINAS Y STRIPCENTER<br>EN LOS TRAPENSES
                        </div>
                    </div>
                    <!-- end: .ms-slide -->
                    <!-- start: .ms-slide -->
                    <div class="ms-slide">
                        <img src="images/blank.gif" data-src="images/home-slider/2.jpg">
                        <img src="images/blank.gif" data-src="images/logo-slider.png" class="ms-layer" data-offset-x="100" data-offset-y="100" data-origin="tl" data-type="image" data-effect="front(long)" data-duration="1500" data-delay="800" data-ease="easeOutExpo">
                        <div class="ms-layer ms-caption" data-offset-x="150" data-offset-y="100" data-origin="bl" data-type="text" data-effect="left(long)" data-duration="1500" data-delay="1200" data-ease="easeOutExpo">
                            NUEVO STRIPCENTER<br>EN LOS TRAPENSES
                        </div>
                    </div>
                    <!-- end: .ms-slide -->
                    <!-- start: .ms-slide -->
                    <div class="ms-slide">
                        <img src="images/blank.gif" data-src="images/home-slider/3.jpg">
                        <img src="images/blank.gif" data-src="images/logo-slider.png" class="ms-layer" data-offset-x="100" data-offset-y="100" data-origin="tl" data-type="image" data-effect="front(long)" data-duration="1500" data-delay="800" data-ease="easeOutExpo">
                        <div class="ms-layer ms-caption" data-offset-x="150" data-offset-y="100" data-origin="bl" data-type="text" data-effect="left(long)" data-duration="1500" data-delay="1200" data-ease="easeOutExpo">
                            OFICINAS HABILITADAS<br>DESDE 22 M<sup>2</sup>
                        </div>
                    </div>
                    <!-- end: .ms-slide -->
                </div>
                <!-- end: #masterslider -->
                <form action="send.php" method="post" data-url="enviar.php" class="formulario wow fadeInRight">
                	<h3>Inscríbete!</h3>
                    <div class="input-wrapper">
                    	<label class="checkbox"><strong></strong></label>
                    	
                    </div>
                    <div class="input-wrapper">
                    	<input type="text" name="nombre" class="inputbox" placeholder="Nombre completo" required>
                    </div>
                    <div class="input-wrapper">
                    	<input type="email" name="email" class="inputbox" placeholder="Email" required>
                    </div>
                    <div class="input-wrapper">
                    	<select name="objetivo" required>
                            <option>Motivo de contacto</option>
                            <option value="Arriendo">Arriendo de locales</option>
                            <option value="Consultas">Consultas</option>
                            <option value="Obtener más información">Obtener más información</option>
                        </select>
                    </div>
                    <input type="hidden" name="action" value="ajax_contact">
                    <div class="msg-exito">Tu mensaje ha sido enviado exitosamente.</div>
                    <input type="submit" value="ENVIAR" class="button submit">
                </form>
            </section>
            <!-- end: #top -->
            
            <header id="header" role="banner">
            	<div class="grid-container">
                    <h1 class="logo"><a href="index.php"><img src="images/logo.png"></a></h1>
                    <nav>
                        <ul>
                            <li><a href="#proyecto">CENTRO ANDINO</a></li>
                            <li><a href="#emplazamiento">EMPLAZAMIENTO</a></li>
                            <li><a href="#entorno">ENTORNO</a></li>
                            <li><a href="#tour">TOUR 360</a></li>
                            <li><a href="#ubicacion">UBICACIÓN</a></li>
                            <li><a href="stripcenter.php">STRIP CENTER</a></li>
                        </ul>
                    </nav>
                    <div class="menu-mobile"></div>
                </div>
                <div class="clear"></div>
            </header>
            
            <!-- start: #proyecto -->
            <section id="proyecto" class="section">
            	<div class="grid-container">
                    <h2 class="titulo wow fadeInDown">COMERCIO <span>A PASOS DE TU HOGAR</span></h2>
                    
                    <div class="item grid-33 tablet-grid-33 wow fadeIn" data-wow-delay="500ms">
                        <h4>EN LOS TRAPENSES</h4>
                        <p>Este exclusivo sector residencial fue el lugar escogido para llevar a cabo un proyecto de clase superior.</p>
                    </div>
                    <div class="item grid-33 tablet-grid-33 wow fadeIn" data-wow-delay="500ms">
                        <h4>STRIPCENTER</h4>
                        <p>26 locales comerciales en 1700 m<sup>2</sup>. Supermercado, servicios y comercio a pasos de tu hogar.</p>
                        <p><a href="stripcenter.php" class="icon">Leer más <img src="images/icono-leermas.png" class="alignmiddle"></a></p>
                    </div>
                    <div class="item grid-33 tablet-grid-33 wow fadeIn" data-wow-delay="500ms">
                        <h4>Lorem ipsum</h4>
                        <p>lorem ipsum dolor sit amte lorem ipsum dolor ist amet lorem ipsum lorem ipsum dolor sit amet.</p>
                        <p><a href="oficinas.php" class="icon">Leer más <img src="images/icono-leermas.png" class="alignmiddle"></a></p>
                    </div>
                </div>
                <div class="info">
                    <div class="img grid-60 tablet-grid-50"></div>
                    <div class="texto alignright grid-40 tablet-grid-50">
                    	<div class="wow fadeInRight" data-wow-delay="500ms">
                            <h2>CENTROANDINO<br><span>MÁS CERCA DE TODO</span></h2>
                            <p>La alta conectividad y facilidad de accesos, son pilares fundamentales en el desarrollo del gran proyecto Centro Andino, un nuevo polo de desarrollo comercial, nace en Los Trapenses.</p>
                            <p>El agrado de estar cerca de todo:<br>Colegios Monte Tabor y Santiago College.<br>Mall Paseo Los Trapenses.<br>Bancos.<br>Supermercados<br>Farmacias.</p>
                        </div>
                    </div>
                <div class="clear"></div>
                </div>
                
            </section>
            <!-- end: #proyecto -->
            
            <!-- start: #emplazamiento -->
            <section id="emplazamiento" class="section grid-container">
            	<h2 class="titulo wow fadeInDown">EMPLAZAMIENTO</h2>
                <a href="images/emplazamiento-hd.jpg" class="lightbox" data-fancybox-type="image"><img src="images/emplazamiento.jpg" class="aligncenter wow fadeIn" data-wow-delay="500ms"></a>
            </section>
            <!-- end: #emplazamiento -->
            
            <!-- start: #servicios -->
            <section id="servicios">
            	<div class="grid-container overflow">
                    <div class="item grid-25 tablet-grid-25 mobile-grid-50 wow fadeInUp">
                        <img src="images/icono-colegios.png">
                        COLEGIOS
                    </div>
                    <div class="item grid-25 tablet-grid-25 mobile-grid-50 wow fadeInUp" data-wow-delay="400ms">
                        <img src="images/icono-paisajismo.png">
                        PAISAJISMO
                    </div>
                    <div class="item grid-25 tablet-grid-25 mobile-grid-50 wow fadeInUp" data-wow-delay="800ms">
                        <img src="images/icono-conectividad.png">
                        CONECTIVIDAD
                    </div>
                    <div class="item grid-25 tablet-grid-25 mobile-grid-50 wow fadeInUp" data-wow-delay="1200ms">
                        <img src="images/icono-malls.png">
                        MALL
                    </div>
                </div>
            </section>
            <!-- end: #servicios -->
            
            <!-- start: #entorno -->
            <section id="entorno" class="galeria section">
            	<div class="grid-container">
                    <h2 class="titulo wow fadeInDown">ENTORNO</h2>
                    <h3 class="bajada wow fadeInDown">Vistas despejadas y en el mejor sector de Los Trapenses</h3>
                    <div class="wow fadeIn" data-wow-delay="500ms">
                        <ul class="tabs">
                            <li><a href="#">IMÁGENES</a></li>
                            <li><a href="#">VIDEO</a></li>
                        </ul>
                        <div class="panes">
                            <!-- start: .tab -->
                            <div class="tab">
                                <div class="slick-slider">
                                    <div class="item"><img src="images/home-entorno/1.jpg"></div>
                                    <div class="item"><img src="images/home-entorno/2.jpg"></div>
                                    <div class="item"><img src="images/home-entorno/3.jpg"></div>
                                    <div class="item"><img src="images/home-entorno/4.jpg"></div>
                                    <div class="item"><img src="images/home-entorno/5.jpg"></div>
                                </div>
                            </div>
                            <!-- end: .tab -->
                            <!-- start: .tab -->
                            <div class="tab">
                                <iframe width="100%" height="477" src="https://www.youtube.com/embed/HTpjzPqASfA?rel=0&showinfo=0&vq=large" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <!-- end: .tab -->
                        </div>
                    </div>
                </div>
            </section>
            <!-- end: #entorno -->
            
            <?php include('templates/tour.php'); ?>
            
            <?php include('templates/ubicacion.php'); ?>
            
        	<div class="clear"></div>
        </main>
        <!-- end: .content -->
		
        <footer id="footer" role="contentinfo">
        	<div class="grid-container">
            	<h3 class="wow fadeInDown">Centro Andino es un proyecto de</h3>
                <ul>
                	<li class="wow fadeIn" data-wow-offset="0" data-wow-delay="500ms"><a href="#"><img src="images/logo_angular.png"></a></li>
                    <li class="wow fadeIn" data-wow-offset="0" data-wow-delay="500ms"><a href="#"><img src="images/logo-ifb-int.png"></a></li>
                </ul>
                <h4>Avenida Pie Andino 5.801, Los Trapenses.</h4>
                <p>Todas las imágenes, ambientaciones y fotos fueron elaborados con fines ilustrativos, no constituyendo necesariamente una representación de la realidad, las dimensiones y superficies son aproximadas.</p>
            </div>
            <a href="#" id="back-to-top"></a>
        </footer>
        
    </div>
    <!-- end: #wrapper -->
    
    <!--[if !IE]><!-->
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript">
    $(window).load(function(){
        if($(document).width() > 980){
            wow = new WOW({
                offset: 150
            });
            wow.init();
        }else{
            $('.wow').removeClass('wow');	
        }
    });
    </script>
    <!--<![endif]-->
    <script type="text/javascript">
    $(window).load(function(){
        $('body').addClass('loaded');
    });
    </script>    
   
</body>
</html>