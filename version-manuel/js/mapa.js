var map;
jQuery(document).ready(function($){
	var icon_url = $('#options').data('marker');
	var lat = $('#options').data('lat');
	var lng = $('#options').data('lng');
	var zoom = $('#options').data('zoom');
	var content = $('#options').html();
	if(lat && lat && zoom)
		initialize(lat, lng, zoom, icon_url, content);
});
function initialize(lat, lng, zoom, icon_url, content) {
	var width = jQuery(document).width();
	var latlng = new google.maps.LatLng(lat, lng);
	var myOptions = {
		zoom: parseInt(zoom),
		center: latlng,
		scrollwheel: false,
		draggable: true,
		zoomControl: true,
		disableDoubleClickZoom: true,
		disableDefaultUI: false,
		//mapTypeId: google.maps.MapTypeId.HYBRID,
		streetViewControlOptions:{
			position: google.maps.ControlPosition.LEFT_BOTTOM,
		},
		zoomControlOptions: {
			position: google.maps.ControlPosition.LEFT_BOTTOM,
			//style: google.maps.ZoomControlStyle.SMALL
		},
/*		styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"administrative.locality","elementType":"all","stylers":[{"saturation":"-2"},{"visibility":"on"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#050505"},{"lightness":17}]}]*/
    };
    map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
	//map.setMapTypeId('SATELLITE');

	var marker = new google.maps.Marker({
		position: latlng,
		map: map,
		//animation: google.maps.Animation.DROP,
		icon: icon_url
	});
	if(content){
		if(width > 700){
			var infowindow = new google.maps.InfoWindow({
				content: '<div class="infowindow">'+content+'</div>',
			});
		}else{
			var infowindow = new google.maps.InfoWindow({
				content: '<div class="infowindow">'+content+'</div>',
				maxWidth: 230
			});
		}
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map, marker);
		});
		infowindow.open(map, marker);
	}
	
	jQuery(window).smartresize(function(){
		map.setCenter(latlng);
		var width = jQuery(document).width();
	});
		
/*	google.maps.event.addDomListener(window, 'resize', function(){
		map.setCenter(latlng);
	});	*/
	
}
