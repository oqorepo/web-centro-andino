jQuery(document).ready(function($){
	$('.formulario').submit(function(){
		var c = i = 0;
		$('form input[type=submit]').attr('disabled', 'disabled').addClass('loading');	
		$(this).find('.empty').removeClass('empty');
		$(this).find('[required]').each(function(){
			if(!$(this).val()){
				$(this).addClass('empty');
				c++;
			}else if($(this).attr('name').indexOf('email', 0) > -1 && $(this).val().indexOf('@', 0) == -1){
				$(this).addClass('empty');
				c++;
				i++;
			}
		});
		// IE fix
		$('.ie input[placeholder], .ie textarea[placeholder]').placeholder();
		if(c == 0)
			$.ajax({
				url: $(this).attr('data-url'),
				type: 'POST',
				beforeSend: function(){ },
				data: $(this).serialize(),
				success: function(data){
					if(data != 'error'){
						$('div.msg-exito').fadeIn().delay(3000).fadeOut();
						$('.formulario input[type=text], .formulario input[type=email], .formulario textarea, .formulario select').val('');
					}else{
						alert("Se ha producido un error intente nuevamente");	
					}	
						
					
					$('.formulario input[type=submit]').attr('disabled', false).removeClass('loading');		
				}
			});
		return false;
	});
	
});