$(window).load(function(){
	
	$('#masterslider').masterslider({
		width: $(window).width(),
		height: ($(window).height()-77),
		space: 0,
		speed: 20,
		instantStartLayers: true,
		layout: 'fullwidth',
		loop: true,
		preload: 0,
		autoplay: true,
		view: 'basic',
/*		controls : {
			arrows : { autohide: false }
		}*/
	});
	
	$('div.slick-slider').slick({
		cssEase: 'ease',
		fade: false,
		arrows: true,
		infinite: true,
		dots: false,
		autoplay: true,
		autoplaySpeed: 4000,
		pauseOnHover: false,
		slidesToShow: 1,
		slidesToScroll: 1
	});
	
});


$(function(){
	deBouncer(jQuery,'smartresize', 'resize', 50);
	deBouncer(jQuery,'smartscroll', 'scroll', 25);	
	
	var document_width = $(document).width();
	
	$('#back-to-top').click(function(event) {
		event.preventDefault();
		$('html, body').animate({scrollTop: 0}, 500);
		return false;
	});
	
	$('#header div.menu-mobile').click(function(){
		$('#header').toggleClass('menu-open');
	});
	
	/* Home */
	
	if(document_width > 1023){
		$('#top').height($(window).height()-77);
		$('#header, div.submenu').sticky();
	}
	
	
	/* Emergentes */
	
	$('.fancybox, map area').fancybox({
		padding: 0,
		margin: 0,
		width: '100%',
		height: '100%',
		type: 'iframe',
		scrolling: 'no',
		autoSize: false,
		fitToView: true,
		nextEffect: 'fade',
		prevEffect: 'fade'
	});
	
	$('.lightbox').fancybox({
		padding: 0,
		scrolling: 'no',
		autoSize: false,
		fitToView: true,
		nextEffect: 'fade',
		prevEffect: 'fade'
	});
	
	$('ul.tabs').not('.noplugin').each(function(){
		$(this).tabs($(this).next().find('div.tab'), {
			history: false,
			effect: 'fade'
		});
		
	});
	
	var offset = -76;
	if($('#proyecto-int').length > 0 && document_width > 1023) offset = -121;
	$.localScroll({ offset: offset, hash: true });
	
	/* Formularios */
	
	$('input[placeholder], textarea[placeholder]').placeholder();
	
	$('form div.input-wrapper .inputbox').focus(function(){
		$('form div.input-wrapper').removeClass('focus');
		$(this).parent().addClass('focus');
	});	
	
	$(window).smartresize(function(){
		if(document_width > 1023)
			$('#top').height($(window).height()-77);
	});
	
	var lastId,
	topMenu = $("#header").not('.interior'),
	topMenuHeight = topMenu.height(),
	menuItems = topMenu.find("a"),
	scrollItems = menuItems.map(function(){
		var item = $($(this).attr("href"));
		if (item.length) { return item; }
	})
	var window_h = $(window).height() - 200;
	
	$(window).smartscroll(function(e){
		var y_scroll_pos = window.pageYOffset;

		if(y_scroll_pos > window_h){
			$('#back-to-top').addClass('visible');
		}else{
			$('#back-to-top').removeClass('visible');
		}
				
		var fromTop = y_scroll_pos+150;
		var cur = scrollItems.map(function(){
		if ($(this).offset().top < fromTop)
			return this;
		});
		cur = cur[cur.length-1];
		var id = cur && cur.length ? cur[0].id : "";
		if (lastId !== id) {
			lastId = id;
			menuItems.removeClass("active").filter("[href=#"+id+"]").addClass("active");
		}
	});
	
});